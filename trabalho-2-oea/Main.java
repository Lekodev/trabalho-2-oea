package cep;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;


public class Main {
	
	public static void main(String[] args) throws Exception {
		criaDatMenor("cep.dat", "newcep.dat", 320);
		ordena("newcep.dat", 320, 30);
	}
	
	
	public static void ordena(String nomeArquivo, int totalRegistros, int tamanhoGrupoRegistros) throws IOException{
		int totalIteracoes = Math.round(totalRegistros / tamanhoGrupoRegistros);
		if((totalRegistros % tamanhoGrupoRegistros) > 0) totalIteracoes++;
		
		int contadorIteracoes = 0;
		
		RandomAccessFile cepFile = new RandomAccessFile(nomeArquivo, "r");
		RandomAccessFile cepFileOrdenado = new RandomAccessFile("ordenado_" + nomeArquivo, "rw");

		cepFile.seek(0);	
		while(contadorIteracoes < totalIteracoes){
			ArrayList<Endereco> lista = new ArrayList<Endereco>();
			 
			for(int i=0; i< tamanhoGrupoRegistros; i++){
				if(cepFile.getFilePointer() == cepFile.length())
					break;
				
				Endereco e = new Endereco();
		        e.leEndereco(cepFile);
		        lista.add(e);			
			}
			
			lista.sort(new CepComparator());
			for(Endereco e : lista){
				e.salvaEndereco(cepFileOrdenado);
			}
			
			contadorIteracoes++;
		}
		
		cepFile.close();
		cepFileOrdenado.close();
	}
	public static void criaDatMenor(String arquivo, String novoArquivo, int qtdRegistros) throws IOException{
		int tamanhoRegistro = 300;
		
		RandomAccessFile cepFile = new RandomAccessFile(arquivo, "r");
		RandomAccessFile newCepFile = new RandomAccessFile(novoArquivo, "rw");
		
		byte[] buffer = new byte[qtdRegistros*tamanhoRegistro];
		cepFile.seek(0);
		cepFile.readFully(buffer);
		newCepFile.write(buffer);
	}

}
