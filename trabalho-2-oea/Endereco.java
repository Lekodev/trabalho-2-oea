package cep;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.charset.Charset;
 
public class Endereco {
     
    private String logradouro;
    private String bairro;
    private String cidade;
    private String estado;
    private String sigla;
    private String cep;
 
    public void leEndereco(DataInput din) throws IOException
    {
        byte logradouro[] = new byte[72];
        byte bairro[] = new byte[72];
        byte cidade[] = new byte[72];
        byte estado[] = new byte[72];
        byte sigla[] = new byte[2];
        byte cep[] = new byte[8];
         
        din.readFully(logradouro);
        din.readFully(bairro);
        din.readFully(cidade);
        din.readFully(estado);
        din.readFully(sigla);
        din.readFully(cep);
        din.readByte(); // Ultimo espaco em branco 
        din.readByte(); // Quebra de linha
         
        // Definie a forma como caracteres especias est�o codificados.
        Charset enc = Charset.forName("ISO-8859-1");
         
        this.logradouro = new String(logradouro,enc);
        this.bairro = new String(bairro,enc);
        this.cidade = new String(cidade,enc);
        this.estado = new String(estado,enc);
        this.sigla = new String(sigla,enc);
        this.cep = new String(cep,enc);
    }
    
    public void salvaEndereco(DataOutput don) throws IOException {
    	
    	Charset enc = Charset.forName("ISO-8859-1");
    	
        byte logradouro[] = new byte[72];
        byte bairro[] = new byte[72];
        byte cidade[] = new byte[72];
        byte estado[] = new byte[72];
        byte sigla[] = new byte[2];
        byte cep[] = new byte[8];
        byte fim[] = {32, 10};
        
        logradouro = this.logradouro.getBytes(enc);
        bairro = this.bairro.getBytes(enc);
        cidade =  this.cidade.getBytes(enc);
        estado =  this.estado.getBytes(enc);
        sigla =  this.sigla.getBytes(enc);
        cep = this.cep.getBytes(enc);
        
        don.write(logradouro);
        don.write(bairro);
        don.write(cidade);
        don.write(estado);
        don.write(sigla);
        don.write(cep);
        don.write(fim);
    }
     
    public String getLogradouro() {
        return logradouro;
    }
     
    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }
     
    public String getBairro() {
        return bairro;
    }
             
    public void setBairro(String bairro) {
        this.bairro = bairro;
    }
     
    public String getCidade() {
        return cidade;
    }
     
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
     
    public String getEstado() {
        return estado;
    }
     
    public void setEstado(String estado) {
        this.estado = estado;
    }
     
    public String getSigla() {
        return sigla;
    }
     
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
     
    public String getCep() {
        return cep;
    }
     
    public void setCep(String cep) {
        this.cep = cep;
    }
 
}