package cep;

import java.util.Comparator;

public class CepComparator implements Comparator<Endereco>{

	@Override
	public int compare(Endereco o1, Endereco o2) {
		int cep1 = new Integer(o1.getCep());
		int cep2 = new Integer(o2.getCep());
		
		if(cep1 > cep2){
			return 1;
		} else if(cep1 < cep2){
			return -1;
		} else {
			return 0;
		}
	}


}
